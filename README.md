# README #

A refactor to patterns of the code challenge "Random Walker" by Daniel Shiffman
https://www.youtube.com/watch?v=l__fEY1xanY

Refactor made with IDE IntelliJ following the tutorial: https://www.processing.org/tutorials/eclipse/

Patterns used:
 - Simple Factory
 - Strategies
 - Decorator