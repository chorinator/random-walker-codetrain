package com.company.Decorators;

import com.company.Walkers.DrawableWalker;
import processing.core.PApplet;

/**
 * Created by luisestebanalonzorivero on 29/01/17.
 */
public class ChangeMagnitude extends WalkerDecorator
{
    private float _rateOfChange, _probability;

    public ChangeMagnitude(PApplet parent, DrawableWalker toBeDecorated, float rateOfChange, float probability)
    {
        super(parent, toBeDecorated);
        _rateOfChange = rateOfChange;
        _probability = probability;
    }

    @Override
    protected void Decorate()
    {
        float prob = _parent.random(0, 100);
        float _magnitude = GetReference().GetProperties().GetMagnitude();

        if (prob < _probability) {
            _magnitude *= _rateOfChange;
        }

        GetReference().GetProperties().SetMagnitude(_magnitude);
    }

}
