package com.company.Decorators;

import com.company.Walkers.DrawableWalker;
import processing.core.PApplet;


public class ChangeStrokeColor extends WalkerDecorator
{
    private int[] colors = new int[3];

    public ChangeStrokeColor(PApplet parent, DrawableWalker toBeDecorated)
    {
        super(parent, toBeDecorated);
        colors[0] = _parent.round(_parent.random(255));
        colors[1] = _parent.round(_parent.random(255));
        colors[2] = _parent.round(_parent.random(255));
    }

    @Override
    protected void Decorate()
    {
        int i = (int)_parent.random(3);
        colors[i] = (colors[i]+1) % 256;
        _parent.stroke(colors[0], colors[1], colors[2]);
    }
}
