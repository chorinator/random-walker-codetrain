package com.company.Decorators;

import com.company.Walkers.DrawableWalker;
import processing.core.*;

public abstract class WalkerDecorator extends DrawableWalker {
    private DrawableWalker _toBeDecorated;

    WalkerDecorator(PApplet parent) {
        super(parent);
    }

    public WalkerDecorator(PApplet parent, DrawableWalker toBeDecorated) {
        super(parent);
        _toBeDecorated = toBeDecorated;
    }

    protected abstract void Decorate();

    @Override
    public final void Walk() {
        Decorate();
        _toBeDecorated.Walk();
    }

    @Override
    public final DrawableWalker GetReference() {
        return _toBeDecorated.GetReference();
    }
}

