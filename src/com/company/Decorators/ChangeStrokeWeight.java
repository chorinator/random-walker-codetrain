package com.company.Decorators;

import com.company.Walkers.DrawableWalker;
import processing.core.PApplet;

/**
 * Created by luisestebanalonzorivero on 29/01/17.
 */
public class ChangeStrokeWeight extends WalkerDecorator
{
    private int _rateOfChange;
    private float _probability;


    public ChangeStrokeWeight(PApplet parent, DrawableWalker toBeDecorated, int rateOfChange, float probability)
    {
        super(parent, toBeDecorated);
        _rateOfChange = rateOfChange;
        _probability = probability;
    }

    @Override
    protected void Decorate()
    {
        float prob = _parent.random(0, 100);
        int _strokeWeight = GetReference().GetStrokeWeight();

        if (prob < _probability) {
            _strokeWeight += _rateOfChange;
            _strokeWeight = _parent.constrain(_strokeWeight, 1, 10);
        }

        GetReference().SetStrokeWeight(_strokeWeight);
    }
}
