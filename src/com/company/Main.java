package com.company;

import com.company.Walkers.DrawableWalker;
import processing.core.*;

import java.util.ArrayList;

public class Main extends PApplet {
    private int width, height, x, y, magnitude, stroke, limit;
    ArrayList<DrawableWalker> walkers = new ArrayList<DrawableWalker>();
    WalkerFactory factory;

    public static void main(String[] args) {
        PApplet.main("com.company.Main");
    }

    public void settings() {
        height = 800;
        width = 800;

        size(width, height);
    }

    public void setup() {
        x = width;
        y = height;
        limit = 100;
        magnitude = 50;
        stroke = 10;
        factory = new WalkerFactory(this);

        background(0);

        DrawableWalker walker = factory.GetRandomWalker(new PVector(random(0, x), random(0, y)), new PVector(x, y), random(1, magnitude), round(random(1, stroke)));
        walkers.add(walker);
    }

    public void draw() {
        if (random(1000) < 1) {
            background(0);
        }

        if (random(100) < 2 && walkers.size() < limit) {
            DrawableWalker walker = factory.GetRandomWalker(new PVector(random(0, x), random(0, y)), new PVector(x, y), random(magnitude), round(random(stroke)));
            walkers.add(walker);
        }

        if (random(100) < 1 && walkers.size() > 1) {
            for (int i = 0; i < random(walkers.size()); i++) {
                int index = round(random(walkers.size() - 1));
                walkers.remove(index);
            }
        }

        for (int i = 0; i < walkers.size(); i++) {
            DrawableWalker walker = walkers.get(i);
            walker.Walk();
        }
    }
}

