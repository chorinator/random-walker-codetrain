package com.company.Behaviours;

import com.company.Walkers.RandomWalker;
import com.company.Walkers.WalkerProperties;
import processing.core.PApplet;
import processing.core.PVector;

public class SquareWalker implements IWakingBehaviour
{
    PApplet  _parent;

    public SquareWalker(PApplet parent) {
        _parent = parent;
    }

    @Override
    public void Walk(WalkerProperties properties) {

        int selector = Math.round(_parent.random(0, 100));
        PVector vector = PVector.random2D().setMag(properties.GetMagnitude());
        vector.x = Math.round(vector.x);
        vector.y = Math.round(vector.y);

        if (selector < 50)
        {
            vector.x = 0;
        } else
        {
            vector.y = 0;
        }

        properties.SetPosition(properties.GetPosition().add(vector));
    }
}