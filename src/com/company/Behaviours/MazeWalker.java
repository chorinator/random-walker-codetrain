package com.company.Behaviours;

import com.company.Walkers.WalkerProperties;
import processing.core.PApplet;
import processing.core.PVector;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;

public class MazeWalker implements IWakingBehaviour {
    private PApplet _parent;
    private int selector;
    private ArrayList<PVector> positions;

    public MazeWalker(PApplet parent) {
        _parent = parent;
        positions = new ArrayList<PVector>();
    }

    enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    @Override
    public void Walk(WalkerProperties properties) {
        PVector vector;

        do {
            selector = Math.round(_parent.random(0, 100));

            if (selector < 25) {
                vector = MoveVector(Direction.UP);
            } else if (selector < 50) {
                vector = MoveVector(Direction.DOWN);
            } else if (selector < 75) {
                vector = MoveVector(Direction.LEFT);
            } else {
                vector = MoveVector(Direction.RIGHT);
            }
            vector = vector.setMag(properties.GetMagnitude());
            vector = vector.add(properties.GetPosition());

            if (!positions.contains(vector))
                break;

            if (NoMoreValidPositions(vector, properties)) {
                SetRandomPosition(properties);
            }

        } while (positions.contains(vector));

        positions.add(vector.copy());
        properties.SetPosition(vector);
    }

    private void SetRandomPosition(WalkerProperties properties) {
        PVector newPos = positions.get((int) _parent.random(positions.size()));
        properties.SetPosition(newPos);
    }

    private PVector MoveVector(Direction direction) {
        switch (direction) {
            case DOWN:
                return new PVector(0, -1);

            case UP:
                return new PVector(0, 1);

            case LEFT:
                return new PVector(-1, 0);

            case RIGHT:
                return new PVector(1, 0);
        }

        throw new NotImplementedException();
    }

    private boolean NoMoreValidPositions(PVector vector, WalkerProperties properties) {
        PVector tester = vector.copy();

        PVector up = tester.add(MoveVector(Direction.UP).setMag(properties.GetMagnitude()));
        PVector down = tester.add(MoveVector(Direction.DOWN).setMag(properties.GetMagnitude()));
        PVector left = tester.add(MoveVector(Direction.LEFT).setMag(properties.GetMagnitude()));
        PVector right = tester.add(MoveVector(Direction.RIGHT).setMag(properties.GetMagnitude()));

        return positions.contains(up) && positions.contains(down) && positions.contains(left) && positions.contains(right);

    }
}
