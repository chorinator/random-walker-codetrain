package com.company.Behaviours;

import com.company.Walkers.WalkerProperties;

public interface IWakingBehaviour {
    void Walk(WalkerProperties properties);
}