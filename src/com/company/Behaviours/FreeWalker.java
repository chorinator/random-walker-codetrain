package com.company.Behaviours;

import com.company.Walkers.RandomWalker;
import com.company.Walkers.WalkerProperties;
import processing.core.PVector;

public class FreeWalker implements IWakingBehaviour {

    @Override
    public void Walk(WalkerProperties properties) {

        PVector newPos = properties.GetPosition().add(PVector.random2D().setMag(properties.GetMagnitude()));
        properties.SetPosition(newPos);

    }
}