package com.company;

import com.company.Behaviours.FreeWalker;
import com.company.Behaviours.IWakingBehaviour;
import com.company.Behaviours.MazeWalker;
import com.company.Behaviours.SquareWalker;
import com.company.Decorators.ChangeMagnitude;
import com.company.Decorators.ChangeStrokeColor;
import com.company.Decorators.ChangeStrokeWeight;
import com.company.Walkers.*;
import processing.core.*;


public final class WalkerFactory {
    private PApplet _parent;

    public WalkerFactory(PApplet parent) {
        _parent = parent;
    }

    public DrawableWalker GetRandomWalker(PVector start, PVector constrains, float mag, int strokeWeight) {
        float selector = _parent.random(100);
        RandomWalker walker = new DrawableWalker(_parent, start, constrains, mag, strokeWeight);

        if (selector < 33) {
            walker.ChangeBehaviour(new FreeWalker());
        } else if (selector < 66) {
            walker.ChangeBehaviour(new SquareWalker(_parent));
        } else {
            walker.ChangeBehaviour(new MazeWalker(_parent));
        }

        return AddRandomDecorators((DrawableWalker) walker);
    }

    private DrawableWalker AddRandomDecorators(DrawableWalker walker) {
        float selector = _parent.random(100);

        if (selector < 50) {
            walker = new ChangeMagnitude(_parent, walker, _parent.random(.9f, 1f), _parent.random(1, 2f));
            walker = new ChangeMagnitude(_parent, walker, _parent.random(1f, 1.1f), _parent.random(1, 2f));
        }

        selector = _parent.random(100);
        if (selector < 50) {
            walker = new ChangeStrokeWeight(_parent, walker, _parent.round(_parent.random(1, 2)), _parent.round(_parent.random(1, 2)));
            walker = new ChangeStrokeWeight(_parent, walker, _parent.round(_parent.random(2)) * -1, _parent.round(_parent.random(2)));
        }

        selector = _parent.random(100);
        if (selector < 50) {
            walker = new ChangeStrokeColor(_parent, walker);
        }

        return walker;
    }
}
