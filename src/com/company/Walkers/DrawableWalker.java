package com.company.Walkers;

import processing.core.PApplet;
import processing.core.PVector;

/**
 * Created by luisestebanalonzorivero on 29/01/17.
 */
public class DrawableWalker extends RandomWalker
{
    private int _strokeWeight;

    public DrawableWalker(PApplet parent) {
        super(parent);
    }

    public DrawableWalker(PApplet parent, PVector start, PVector constrains, float mag, int strokeWeight)
    {
        super(parent, start, constrains, mag);
        _strokeWeight = strokeWeight;
        _parent.stroke(255);
    }

    public void SetStrokeWeight(int w)
    {
        _strokeWeight = w;
        _parent.strokeWeight(_strokeWeight);
    }

    public int GetStrokeWeight()
    {
        return _strokeWeight;
    }

    //Decorator's recursion end
    public DrawableWalker GetReference()
    {
        return this;
    }

    @Override
    public void Walk()
    {
        super.Walk();

        PVector current = _properties.GetPosition();
        PVector last = _properties.GetPreviousPosition();

        _parent.noFill();
        _parent.beginShape();

        _parent.vertex(last.x, last.y);
        _parent.vertex(current.x, current.y);

        _parent.endShape();
    }

}
