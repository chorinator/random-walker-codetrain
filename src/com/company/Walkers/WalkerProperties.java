package com.company.Walkers;

import processing.core.PApplet;
import processing.core.PVector;

public class WalkerProperties {
    private float _magnitude;
    private PVector _constrains;
    private PVector _position;
    private PVector _previousPosition;

    public WalkerProperties() {
        _constrains = new PVector();
        _position = new PVector();
        _previousPosition = new PVector();
        _magnitude = 1;
    }

    public WalkerProperties(PVector consrains, PVector position, float magnitude) {
        _constrains = consrains;
        _position = position;
        _previousPosition = position.copy();
        _magnitude = magnitude;
    }

    public void SetMagnitude(float mag) {
        _magnitude = mag;
    }

    public float GetMagnitude() {
        return _magnitude;
    }

    public PVector GetPosition() {
        return _position.copy();
    }

    protected PVector GetPreviousPosition() {
        return _previousPosition.copy();
    }

    public final void SetPosition(PVector position) {
        _previousPosition.set(_position);
        _position.set(position);

        AdjustBounds();
    }

    private void AdjustBounds() {
        _position.x = PApplet.constrain(_position.x, 0, _constrains.x);
        _position.y = PApplet.constrain(_position.y, 0, _constrains.y);
    }

    public PVector GetConstrains() {
        return _constrains;
    }

    public void SetContrains(PVector constrains) {
        _constrains = constrains;
    }
}
