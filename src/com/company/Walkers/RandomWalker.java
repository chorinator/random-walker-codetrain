package com.company.Walkers;

import com.company.Behaviours.IWakingBehaviour;
import processing.core.*;

public abstract class RandomWalker {

    protected PApplet _parent;
    protected IWakingBehaviour _behaviour;
    protected WalkerProperties _properties;

    RandomWalker(PApplet parent) {
        _parent = parent;
    }

    RandomWalker(PApplet parent, PVector start, PVector constrains, float mag) {
        _parent = parent;
        _properties = new WalkerProperties();
        _properties.SetContrains(constrains);
        _properties.SetPosition(start);
        _properties.SetMagnitude(mag);
    }

    RandomWalker(PApplet parent, WalkerProperties properties, IWakingBehaviour behaviour) {
        _parent = parent;
        _properties = properties;
        _behaviour = behaviour;
    }

    public void ChangeBehaviour(IWakingBehaviour behaviour) {
        _behaviour = behaviour;
    }

    public void Walk() {
        _behaviour.Walk(_properties);
    }

    public WalkerProperties GetProperties() {
        return _properties;
    }

}

